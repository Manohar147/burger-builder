import React from 'react';
import Aux from '../../HOC/Auxiliary';
import * as LayoutClasses from './Layout.module.css';

const layout = (props) => (
    <Aux>
        <div>
            Toolbar,SideNav,Badckdrop
        </div>
        <main className={LayoutClasses.Content}>
            {props.children}
        </main>
    </Aux>
);

export default layout;
