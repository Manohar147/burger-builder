import React from 'react';
import Aux from '../../HOC/Auxiliary';

export default class BurgerBuilder extends React.Component {
    render() {
        return (
            <Aux>
                <div>Burger</div>
                <div>Build Controls</div>
            </Aux>
        );
    }

}
